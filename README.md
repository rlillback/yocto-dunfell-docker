## yocto-dunfell-docker

* A docker container to use the Yocto Project to build Linux images
* This container relies on Ubuntu 18.04 LTS as a starting image

## Copy credentials for use inside the container

	cd yocto-dunfell-docker
	cp ~/.ssh/id* docker/

## Build Image

	./start-yocto-build
